/*! jQuery.niceSelect.js - created by Jennie Ji and modified by iRamesh Thakulla
 *   Latest update: 2013-09-08
 */
(function($){
    $.fn.niceSelect = function( options ) {
        var $selects = $(this);
        var selectsClass = $selects.attr('class');
        var defaults = {
            zindex: 10
        },opts = $.extend(defaults, options);

        var PREFIX = 'fancy-select';
        var CLASS = {
                wrapper:    PREFIX + 'wrapper',
                select:     PREFIX + 'select',
                selectText: PREFIX + 'label',
                selectOpen: PREFIX + 'open',
                dropdown:   PREFIX + 'dropdown',
                disabled:   PREFIX + 'disabled'
            },
            SELECTOR = {};

        var wrapper = '<div class="' + CLASS.wrapper + '" />';

        // functions
        var isInited = function( $select ){
            return $select.parent( SELECTOR.wrapper ).length>0;
        }
        var destroy = function( ){
            $selects.each(function(){
                var $select = $(this);
                if( !isInited( $select ) ) { return; }
                $selects.siblings().remove();
                $selects.unwrap().show();
            });
            return this;
        };
        var hideSelect = function(){
            $(SELECTOR.dropdown).hide();
        };
        var init = function( ){
            if( $selects.length<1 ) { return; }

            $selects.each(function(){
                var $select = $(this);
                if( isInited( $select ) ) { return; }

                var $options = $select.find('OPTION');
                var $selected = $select.find(':selected').length<1 ? $select.first() : $select.find(':selected');
                var $newSelect, $newSelectList, $newSelectText, $newSelectOpen;

                var selectWidth = $select.outerWidth(),
                    selectHeight = $select.outerHeight();
                var isDisabled = $select.is(':disabled');

                // build HTML
                // var newSelect = '<div class="fancy-select ' + CLASS.select + ( isDisabled ? ' niceSelect-disabled' : '' ) + ' '+ selectsClass +'"></div>',
                var newSelect = '<div class="fancy-select ' + CLASS.select + ' '+ selectsClass +'"></div>',
                    newSelectOpen = '<span class="' + CLASS.selectOpen + '"></span>',
                    newSelectText = '<span class="' + CLASS.selectText + ' current">' + $selected.text() + '</span>',
                    newDropdown = function(){
                        return '<ul class="fancy-list ' + CLASS.dropdown + ' "></ul>';
                    };

                // append HTML
                $select.hide().wrap( wrapper );
                if(isDisabled) {
                    $select.parent().addClass('disabled-wrapper')
                }else {
                    $select.parent().removeClass('disabled-wrapper')
                }
                $newSelect = $(newSelect).insertAfter($select);
                //$newSelectOpen = $( newSelectOpen ).appendTo( $newSelect );
                $newSelectText = $( newSelectText ).appendTo( $newSelect );

                // set style
                // $newSelectText.width( selectWidth - $newSelectOpen.outerWidth(true) - $newSelect.outerWidth() );


                // if(!isDisabled){                    
                // }
                // build dropdown
                $newSelectList = $( newDropdown() ).appendTo( $newSelect );
                var attrVal;
                $options.each(function(){
                    $('<li class="option"><a href="#">' + $(this).text() + '</a></li>')
                        .appendTo( $newSelectList )
                        .data('value', $(this).val() );
                });
                // open the dropdown list
                $newSelect.click( function(){
                    var $self = $(this);
                    if(!$self.hasClass('open')) {
                        $newSelect.addClass('open');
                    }else {
                        $newSelect.removeClass('open');
                    }
                });

                //Highlight selected option
                function selectedOption() {
                    $select.find('option:selected').each(function(index, elem) {
                        var thisIndex = $(elem).index(),
                            thisVal = $(elem).val();
                        //console.log(thisVal + ' ' + thisIndex);
                        $select.next('.fancy-select').find('ul > li:eq('+ thisIndex +')').each(function() {
                            var $self = $(this);
                            $select.next('.fancy-select').find('ul > li').removeClass('filtered selected');
                            $self.addClass('filtered selected');
                        });
                        //console.log(thisIndex)
                    });
                }

                //Init selected option highlight
                selectedOption();

                // change the select's value
                $newSelectList.delegate('A','click',function(e){
                    e.preventDefault();
                    $newSelectText.html( $(this).text() );
                    $select.val( $(this).parent().data('value') );

                    $select.change();
                    //Init selected option highlight
                    selectedOption();

                    //hideSelect();
                });
            });
            // select blur effect
            $(document).mousedown(function(e) {
                if ($(e.target).parents( SELECTOR.wrapper ).length === 0) {
                    //hideSelect();
                }
            });

            return this;
        };
        var refresh = function(){
            destroy();
            init();
            return this;
        };

        // init SELECTOR
        $.each( CLASS, function(key, val){
            SELECTOR[key] = '.' + val;
        });

        init();

        return {
            refresh: refresh,
            destroy: destroy
        };

    };
})( jQuery );
