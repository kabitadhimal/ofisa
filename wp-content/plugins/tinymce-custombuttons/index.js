(function() {
    tinymce.create("tinymce.plugins.green_button_plugin", {

        //url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            //add new button     
            ed.addButton("custom_button", {
                title : "Custom Button",
                cmd : "custom_button",
                image : "https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/32x32/Circle_Green.png"
            });

            //button functionality.
            ed.addCommand("custom_button", function() {
                var return_text = "[custom_button link='' target=''][/custom_button]";
                ed.execCommand("mceInsertContent", 0, return_text);
            });

        }
    });

    tinymce.PluginManager.add("green_button_plugin", tinymce.plugins.green_button_plugin);
})();