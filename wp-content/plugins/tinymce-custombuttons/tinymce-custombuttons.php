<?php

/*
Plugin Name: Visual Editor Buttons
Description: Adds a button to visual editor.
Author: Subodh Dhukuchhu
*/


function enqueue_plugin_scripts($plugin_array)
{
    //enqueue TinyMCE plugin script with its ID.
    $plugin_array["green_button_plugin"] =  plugin_dir_url(__FILE__) . "index.js";
    return $plugin_array;
}

add_filter("mce_external_plugins", "enqueue_plugin_scripts");

function register_buttons_editor($buttons)
{
    //register buttons with their id.
    array_push($buttons, "custom_button");
    return $buttons;
}

add_filter("mce_buttons", "register_buttons_editor");

