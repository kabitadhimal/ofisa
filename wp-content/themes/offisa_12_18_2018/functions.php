<?php
function redirect_not_found_page_to_homepage(){
    if( is_404() ){
        $current_url = urldecode ("//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        if (strpos($current_url, '/net/Info.asp?NoOFS=8105&NumStr=home') !== false) {
            $url = home_url();
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location: $url"); 
            exit();
        }
    }
}
add_action( 'template_redirect', 'redirect_not_found_page_to_homepage' );

define('TEMPATH', get_bloginfo('template_directory'));
define('IMAGE', TEMPATH . "/images");
define('CSS', TEMPATH . "/css");
define('SCRIPT', TEMPATH . "/js");
require get_template_directory() . '/inc/init.php';