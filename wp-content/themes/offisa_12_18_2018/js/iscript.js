/* ============================================================= */
/*      Ramesh Thakulla || Procab Front-End/UI/UX Developer      */
/*      Custom javascript codes to be used in Ofisa project      */
/* ============================================================= */

/* ============================================================= */
/*    Initializing global variables to run throughout the site   */
/* ============================================================= */

// Clear timeout variables in window resize
var resizeTimer,
    resizeTimerDelay = 100;

// Check if the device is ipad/tablet/smartphones
var isGadget = isDeviceCheck();

/* ============================================================= */
/*     Initializing Function/Methods before page/window load     */
/* ============================================================= */

// Init custom accordion
ofisaAccordion($('.ofisa__accordion'), $('.ofisa__accordion-panel .toggler'));
// Init main slider
ofisaMainSlider($('.ofisa__full-slider'));
// Init placehoder hide seek
placeholderHideSeek($('.form-control'));
// Init converting select box to fancy dropdown
convertToFancyDropDownUi($('select.ofisa__form-control'));
// Init mobile menu
mobileMenuOpener($('.navbar-toggle'));
// Init data-title attr to table data cell
dataTitleAttr($('table tr'));
// Init gadgetStuffs
gadgetStuffs();
// Init sticky header
stickyHeader();
// Init parallax effect
buildParallax($('[data-parallax*="true"]'));
// Init hoverClasses
hoverClasses($('section[data-hover="true"]'), $('.js-hover-box'));
// Init animateDomElements
animateDomElements($('.js-animation'));
// Init scrollPageTop
scrollPageTop($('.ofisa__backTop'));
// Init videoPlayer
videoPlayer($('[data-video-player="true"]'));
// Init ofisaVerticalAccordion
ofisaVerticalAccordion($('.ofisa__vertical-accordion'), $('.vAccor-toggle'));
// Init checkListGroup
checkListGroup($('ul'));
// Init wrapIframe
wrapIframe($('iframe'));

/* ============================================================= */
/*        Initializing Function/Methods on document load         */
/* ============================================================= */

/* ============================================================= */
/*         Initializing Function/Methods on window load          */
/* ============================================================= */

/* ============================================================= */
/*        Initializing Function/Methods on window resize         */
/* ============================================================= */

$(window).on('resize', function() {
   if(resizeTimer !== null) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
        //Init data-title attr to table data cell
        dataTitleAttr($('table tr'));
        //Init gadgetStuffs
        gadgetStuffs();
        // Init sticky header
        stickyHeader();
    }, resizeTimerDelay);
});

// Function to build custom vertical accordion
function ofisaVerticalAccordion($accordion, $item) {
    if($item.length === 0) return;
    var accordionDataOption = $item.closest($accordion).attr('data-collapse-option');
    $item.each(function() {
       var $self = $(this),
           $parent = $self.closest('.vertical-accordion-panel'),
           $target = $('.' + $self.attr('data-panel'));
       $self.on('click', function (e) {
           if(!$self.hasClass('active')) {
               $self.addClass('active').removeClass('inactive');
               $parent.addClass('active-panel');
               $target.stop(true).slideToggle(300);
               if(accordionDataOption === 'single' && $('.vAccor-toggle.active').length >= 1) {
                   $parent.siblings().removeClass('active-panel');
                   $parent.siblings().find('.vertical-accordion-group').stop(true).slideUp(300);
                   $parent.siblings().find('.active').removeClass('active').addClass('inactive');
               }
               // Trigger the first list of each group
               // $self.next($('.' + $self.attr('data-panel'))).find('>li:first-of-type > .vAccor-panel-toggle').trigger('click');
               $('.' + $self.attr('data-panel')).find('>li:first-child > .vAccor-panel-toggle').trigger('click');
           }else if($self.hasClass('active')) {
               $self.addClass('inactive').removeClass('active');
               $parent.removeClass('active-panel');
               $target.stop(true).slideToggle(300);
           }
           e.preventDefault();
       });
    });
}

// Function to create a custom accordion
function ofisaAccordion($accordion, $item) {
    if($item.length === 0) return;
    var winWidth = $(window).innerWidth(),
        $header = $('#masthead'),
        headerHeight = $header.outerHeight(true),
        accordionTimer,
        accordionTimerDealy = 300,
        $accordionInit = $($accordion),
        accordionDataOption = $item.closest($accordionInit).attr('data-collapse-option');


    var $activePanel = $accordionInit.find('.ofisa__accordion-panelBody.active');
    $activePanel.closest('.ofisa__accordion-panel').siblings().find('.ofisa__accordion-panelBody').hide();
    $activePanel.show();

    $item.each(function() {
        var $self = $(this),
            $parent = $self.closest('.ofisa__accordion-panel');
        $(window).on('resize', function() {
            clearTimeout(accordionTimer);
            accordionTimer = setTimeout(function() {
                headerHeight = $header.outerHeight(true);
            }, accordionTimerDealy);
        });
        //Attach click event
        $self.on('click', function(e) {
            var $panelBody = $parent.find('.ofisa__accordion-panelBody'),
                headerHeight = $header.outerHeight(true);
            if($self.hasClass('opener')) {
                $self.removeClass('opener').addClass('closer');
                $($accordion).addClass('accordion-on').removeClass('accordion-off');
                $parent.siblings().removeClass('active-panel');
                $parent.addClass('active-panel');
                $panelBody.stop(true).end().slideDown(300, function() {
                    $panelBody.addClass('active');
                });
                setTimeout(function() {
                    $('html, body').animate({
                        //scrollTop: $parent.offset().top - (headerHeight) - 1
                    }, {
                        duration: 1200,
                        easing: 'easeInOutExpo'
                    });
                }, 300);
            }else if($self.hasClass('closer')) {
                $self.addClass('opener').removeClass('closer');
                $($accordion).removeClass('accordion-on').addClass('accordion-off');
                $parent.removeClass('active-panel');
                $panelBody.stop(true).slideUp(300, function() {
                    $panelBody.removeClass('active');
                });
            }
            /* Feature to close other panels */
            //Check if more than one panel is opened
            var $opener = $('.toggler.closer');
            if(accordionDataOption === "single" || accordionDataOption === undefined || accordionDataOption === null) {
                if($opener.length >= 1) {
                    //console.log($opener.length)
                    $parent.siblings().find('.ofisa__accordion-panelBody').stop(true).slideUp(300, function() {
                    });
                    $panelBody.stop(true).slideDown(300, function() {
                        $parent.siblings().find('.ofisa__accordion-panelBody').removeClass('active');
                        $panelBody.addClass('active');
                    });
                    $parent.siblings().find('.toggler').removeClass('closer').addClass('opener');
                }
            }else if(accordionDataOption === "multiple") {
                return false;
            }
            e.preventDefault();
        });
    });
}

// Function to activate a standalone common slider over the site
function ofisaMainSlider($slider) {
    if($slider.length === 0) return;
    var sliderTimer,
        sliderTimerDelay = 350;
    $slider.each(function() {
        var $self = $(this),
            autoPlaySpeed = parseInt($self.attr('data-autoplay-speed')),
            $selfSlider = new Swiper($self, {
                autoplay: autoPlaySpeed,
                speed: 800,
                pagination: '.ofisa__slider-pagination',
                paginationClickable: true,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                slidesPerView: '1',
                // scrollbarDraggable: false,
                // resizeEvent: 'resize',
                // autoResize: true,
                // initialSlide: 0,
                autoplayDisableOnInteraction: false,
                preventClicksPropagation: false,
                preventClicks: false,
                preloadImages: false,
                lazyLoading: true,
                //Call back funciton on slider init
                onInit: function(Swiper) {
                    var $swiperContainer = Swiper.container,
                        $swiperWrapper = Swiper.wrapper,
                        $swiperSlide = Swiper.slides,
                        $swiperDisabledButton = $self.find('.ofisa__slider-btn');
                        $swiperPagination = $self.find('.swiper-pagination');
                    if($swiperSlide.length <= 1) {
                        $swiperDisabledButton.addClass('hidden');
                        $swiperPagination.addClass('hidden');
                        $swiperWrapper.addClass('transformation__off');
                    }

                    if($swiperContainer.hasClass('ofisa__slider-loading')) {
                        $swiperContainer.removeClass('ofisa__slider-loading');
                    }
                }
            });
        //Update slider on resize
        $(window).on('resize', function() {
            if(sliderTimer !== null) clearTimeout(sliderTimer);
            sliderTimer = setTimeout(function() {
                // Update slider on resize
                $selfSlider.update();
            }, sliderTimerDelay);
        });
    });
}

// Function to hide and show form input field placeholder values
function placeholderHideSeek(input) {
    if($(input).length === 0) return;
    input.each( function(){
        var meInput = $(this);
        var placeHolder = $(meInput).attr('placeholder');
        $(meInput).focusin(function(){
            $(meInput).attr('placeholder','');
        });
        $(meInput).focusout(function(){
            $(meInput).attr('placeholder', placeHolder);
        });
    });
}

//Function to create mobile menu
function mobileMenuOpener($menu) {
    if($menu.length === 0) return;
    $menu.each(function() {
        var $self = $(this),
            $parent = $self.closest('nav'),
            $html = $('html');
        //Attch click event
        $self.on('click', function(e) {
            var $target = $self.closest('header').find('.' + $self.attr('data-target'));
            if($self.hasClass('closer')) {
                $self.removeClass('closer').addClass('opener');
                $target.addClass('active-menu').stop(true).slideDown({
                    duration: 700,
                    easing: 'easeInOutExpo'
                });
                $parent.addClass('active-nav');
                $html.addClass('lock-html no-scroll');

            }else if($self.hasClass('opener')) {
                $self.addClass('closer').removeClass('opener');
                $target.removeClass('active-menu').slideUp({
                    duration: 1000,
                    easing: 'easeInOutExpo'
                });
                $parent.removeClass('active-nav');
                $html.removeClass('lock-html no-scroll');
            }
            e.preventDefault();
        });
    })
}

//Function to convert default select boxes to fancy select boxes
function convertToFancyDropDownUi($select) {
    if($select.length === 0) return;
    $select.each(function() {
        //Caching jquery object in variables
        var $self = $(this);
        //Attaching niceselect custom plugin to each current object
        $self.niceSelect().refresh();
        //Injecting nicescroll on dropdown boxes
        var $fancySelect = $self.next('.fancy-select');
        //Wrapping each fancy select object with div
        var $fancyList = $fancySelect.find('ul.fancy-list');
        $fancySelect.each(function() {
            var $selfFancy = $(this),
                $fancyList = $selfFancy.find('ul.fancy-list');
            $selfFancy.on('click', function(e) {
                $selfFancy.closest('.ofisa__inline-group').siblings().find('.fancy-select').removeClass('open');
                $('ul.fancy-list').getNiceScroll().remove();
                if($selfFancy.hasClass('open')) {
                    setTimeout(function () {
                        $fancyList.niceScroll({
                            cursorcolor: "#cccccc",
                            cursoropacitymin: 0.5,
                            background: "#f0f0f0",
                            cursorborder: "0",
                            autohidemode: false,
                            cursorwidth: 10,
                            cursorborderradius: 5,
                            cursorminheight: 50,
                            cursorfixedheight: 50,
                        });
                    }, 100);

                    setTimeout(function () {
                        $fancyList.getNiceScroll().show().resize();
                    }, 300);

                    $(document).on('click', function(e) {
                        var container = $('.fancy-select');
                        if (!container.is(e.target) // if the target of the click isn't the container...
                            && container.has(e.target).length === 0) // ... nor a descendant of the container
                        {
                            $('ul.fancy-list').getNiceScroll().remove();
                            $('.fancy-select').removeClass('open');
                        }
                    });

                }else if(!$self.hasClass('open')) {
                    $fancyList.getNiceScroll().remove();
                }
            });
        });
    });
}

//Function to add data-title attribute to table data cell on smaller screen
function dataTitleAttr($tr) {
    if($tr.length === 0) return;
    $tr.each(function(index, element) {
        var $self = $(this),
            $th = $self.find('th'),
            winWidth = $(window).innerWidth();
        if(winWidth <= 767 - 17) {
            $self.addClass('cms-table-tr' + $self.index())
            $th.each(function(index, element) {
                var $selfTh = $(this),
                    selfThIndex = $selfTh.index(),
                    selfThTitle = $selfTh.text();
                $self.closest('table').find('tr td:eq('+selfThIndex+')').attr('data-title', selfThTitle);
            });
            setTimeout(function() {
                $('tbody tr.cms-table-tr0 td').each(function(index, element) {
                    var $selfTd = $(this),
                        selfTdIndex = $selfTd.index(),
                        selfTdDataTitle = $selfTd.attr('data-title'),
                        $selfTdParent = $selfTd.closest('tr');
                    //console.log(selfTdDataTitle)
                    $selfTdParent.siblings().find('td:eq('+selfTdIndex+')').attr('data-title', selfTdDataTitle);
                });
            }, 300);
        }else if(winWidth > 767 - 17) {
            $('table tr').removeClass();
            $('table tr > td').removeAttr('data-title');
        }
    });
}

//Function to do different stuffs in gadgets
function gadgetStuffs() {
    var $body = $('body'),
        $win = $(window),
        winWidth = $win.innerWidth();
    if(isGadget) {
        if(winWidth > 1024) {
            $body.removeClass('disable-hover-interaction mouseinout-interaction-off gadget-detected');
        }else if(winWidth <= 1024) {
            $body.addClass('disable-hover-interaction mouseinout-interaction-off gadget-detected');
        }
    }else {
        if(winWidth > 1024 - 17) {
            $body.removeClass('disable-hover-interaction mouseinout-interaction-off gadget-detected');
        }else if(winWidth <= 1024) {
            if(isGadget) {
                $body.addClass('disable-hover-interaction mouseinout-interaction-off gadget-detected');
            }
        }
    }
}

// Function isDevice check
function isDeviceCheck() {
    var isiDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase());
    return isiDevice;
}

// Function sticky header
function stickyHeader() {
    if($('#masthead').length === 0) return;
    var $header = $('#masthead'),
        headerHeight = $header.outerHeight(true),
        $window =  $(window),
        windowOffset = $window.scrollTop();
    headerOffset = $header.offset().top;

    if(windowOffset >= headerHeight && $('body').hasClass('ofisa-homepage-template')) {
        $header.addClass('ofisa__sticky');
    }

    $window.on('scroll', function() {
        windowOffset = $window.scrollTop();
        if($('body').hasClass('ofisa-homepage-template')) {
            if(windowOffset >= headerHeight) {
                $header.addClass('ofisa__sticky');
            }else {
                $header.removeClass('ofisa__sticky');
            }
        }
    });
}

// Function to create simple but smooth parallax effects
function buildParallax($section) {
    if($section.length === 0 || typeof($section) === undefined) return;
    var $window = $(window),
        winScrollTop = $window.scrollTop();
    $('<div>', {'class': 'ofisa__parallax-fixer'}).prependTo('body');
    $section.each(function() {
       var $self = $(this),
           dataSpeed = $self.data('speed'),
           dataDelay = $self.data('delay');

        $window.on('scroll', function() {
            winScrollTop = $window.scrollTop();
            if(isScrolledIntoView($self)) {
                $self.css({
                    //'transform': 'translateY('+ (-(winScrollTop * 0.2) / dataSpeed) + 'px' +')'
                    backgroundPosition: '50%' + -(winScrollTop * dataDelay) / (dataSpeed * 4) + 'px'
                }).
                next().
                css({
                   marginTop: -(winScrollTop * dataDelay) / (dataSpeed * 4) + 'px',
                });
                
                var $parallaxFixer = $('.ofisa__parallax-fixer');
                if($parallaxFixer.length <= 1) {
                    $parallaxFixer.css({
                        'height': -(parseInt($self.next().css('margin-top'))) + 'px'
                    }); 
                }
            }
        });
    });
}

// Function to create simple but smooth parallax effects
function animateDomElements($boxes) {
    if($boxes.length === 0 || typeof($boxes) === undefined) return;
    var $window = $(window),
        winScrollTop = $window.scrollTop();
    $boxes.each(function() {
        var $self = $(this),
            dataSpeed = $self.data('speed'),
            dataAnimationClass = $self.data('animation');

        if(isScrolledIntoView($self)) {
            $self.addClass(dataAnimationClass);
        }

        $window.on('scroll', function() {
            winScrollTop = $window.scrollTop();
            if(isScrolledIntoView($self)) {
                // $self.css({
                //     marginTop: (-(winScrollTop * 0.2) / dataSpeed) + 'px'
                // });
                $self.addClass(dataAnimationClass);
            }
        });
    });
}

// Function to add hover class in home page boxes
function hoverClasses($section, $boxes) {
    if($boxes.length === 0 || typeof($boxes) === 'undefined') return;
    $boxes.each(function() {
       var $self = $(this),
           $parent = $self.closest($section);
       $self.hover(function() {
           $parent.addClass('hover-on');
           $self.addClass('hovered-box');
       }, function() {
           $parent.removeClass('hover-on');
           $self.removeClass('hovered-box');
       });
    });
}

//Function to play youtube video on inline place
function videoPlayer($elem) {
    if($elem.length === 0) return;
    $elem.each(function() {
        var $self = $(this),
            $selfParent = $self.closest('.ofisa__video-row'),
            $selfWrapper = $selfParent.find('.ofisa__video-wrapper'),
            $targetIframe = $selfParent.find('iframe'),
            selfDataUrl = $self.attr('data-video-src'),
            $bkgImg = $selfParent.find('.ofisa__video-cover'),
            youtubeMainString = 'https://www.youtube.com/embed/',
            youTubeVideoPlayParams = '?modestbranding=1&rel=0&showinfo=0&autoplay=1&autohide=1&controls=1&loop=1',
            vimeoMainString = 'https://player.vimeo.com/video/',
            vimeoVideoPlayParams = 'autoplay=1&fullscreen=1&title=0&byline=0;';

        // Caching values extracted via parseVideourl function
        var videoSource = parseVideoURL(selfDataUrl),
            videoProvider= videoSource.provider,
            loadedVideoId = videoSource.id;
        //console.log(loadedVideoId)
        $self.on('click', function(e) {
            //Hide video banner image and play button
            $self.addClass('loading');
            $selfWrapper.addClass('video-playing');
            $self.hide();
            $bkgImg.hide();
            if( videoProvider == 'youtube' ) {
                //Assign youtube url to the iframe on modal
                $targetIframe.addClass('video-playing').attr({'src' : youtubeMainString + loadedVideoId + '?' + youTubeVideoPlayParams + '&playlist=' + loadedVideoId + ''}).load(function() {
                    //Hide video banner image and play button
                    $self.removeClass('loading');
                    $selfWrapper.addClass('video__player-is__running');
                });
                //Display current youtube url on //console
                console.log("<=======" + ' ' + videoProvider + ' ' + 'video is playing!!!' + ' ' + "=======>" );
            }else if( videoProvider == 'vimeo' ) {
                //Assign youtube url to the iframe on modal
                $targetIframe.addClass('video-playing').attr({'src' : vimeoMainString + loadedVideoId + '?' + vimeoVideoPlayParams}).load(function() {
                    //Hide video banner image and play button
                    $self.removeClass('loading');
                    $selfWrapper.addClass('video__player-is__running');m
                });
                //Display current vimeo url on //console
                console.log("<=======" + ' ' + videoProvider + ' ' + 'video is playing!!!' + ' ' + "=======>" );
            }
            e.preventDefault();
        });
    });
}

// Function to scroll to the top from the bottom of the page
function scrollPageTop($elem) {
    if($elem.length === 0) return;
    var $window = $(window),
        winScrollTop,
        winHeight = $window.height();

    $window.on('scroll', function() {
        winScrollTop = $window.scrollTop();
        if(winScrollTop >= 150) {
            $elem.closest('.ofisa__backTop-wrapper').addClass('animate');
        }else {
            $elem.closest('.ofisa__backTop-wrapper').removeClass('animate');
        }
    });

    $elem.each(function() {
        var $self = $(this);
        //Attach click event on current object
        $self.on('click', function(e) {
            $('html, body').animate({scrollTop: 0}, {duration: 1200,easing: 'easeInOutExpo'});
            // Prevent the default behavior of the anchor tag
            e.preventDefault();
        });
    });
}

// Function to check if ul has a
function checkListGroup($ul) {
    if($ul.length === 0) return;
    $ul.each(function() {
        var $self = $(this);
        if(($self.closest('header').length === 0 || $self.closest('footer').length === 0) && $self.attr('class') === undefined) {
            $self.addClass('is__list-group');
        }else {
            $self.removeClass('not__list-group');
        }
    });
}

// Function to add each closest parent of iframe with some classes
function wrapIframe($iframe) {
    if($iframe.length === 0) return;
    var $wrapper = $('<span>', {'class': 'ofisa__naked-responsive-wrap'});

    $iframe.each(function(index, element) {
        var $self = $(this);
        //$wrapperObj = $self.find('.lesroch__cms-naked-responsive-wrap');
        if($self.parent().attr('class') === undefined) {
            $self.addClass('ofisa__responsive-video').wrap($wrapper);
        }
    });
}