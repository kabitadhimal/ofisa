/* ============================================================= */
/*      Ramesh Thakulla || Procab Front-End/UI/UX Developer      */
/*      Custom javascript codes to be used in Ofisa project      */
/* ============================================================= */

/* ============================================================= */
/*    Initializing global variables to run throughout the site   */
/* ============================================================= */

//Parse video host name and vide id from the given url
function parseVideoURL(url) {
    function getParm(url, base) {
        var re = new RegExp("(\\?|&)" + base + "\\=([^&]*)(&|$)");
        var matches = url.match(re);
        if (matches) {
            return(matches[2]);
        } else {
            return("");
        }
    }
    var retVal = {};
    var matches;
    if (url.indexOf("youtube.com/watch") != -1) {
        retVal.provider = "youtube";
        retVal.id = getParm(url, "v");
    } else if (matches = url.match(/vimeo.com\/(\d+)/)) {
        retVal.provider = "vimeo";
        retVal.id = matches[1];
    }else if(matches = url.split('?')[0].split("/")) {
        retVal.provider = "vimeo";
        retVal.id = matches[matches.length - 1];
    }
    return(retVal);
}

// Function to check if an element is on viewport
function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).outerHeight(true);

    //return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    //return ((elemBottom >= docViewBottom) && (elemTop <= docViewTop)) || ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}