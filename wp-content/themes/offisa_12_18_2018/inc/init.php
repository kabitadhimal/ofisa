<?php

/**
 * Custom Class folder
 */
require get_template_directory() . '/inc/class/class-timber.php';
require get_template_directory() . '/inc/class/class-offisa.php';

/**
 * Custom functionsfolder.
 */
require get_template_directory() . '/inc/function/function-extras.php';
require get_template_directory() . '/inc/function/function-sd.php';
require get_template_directory() . '/inc/function/function-template.php';
require get_template_directory() . '/inc/function/function-shortcodes.php';
require get_template_directory() . '/inc/ajax/ajax.php';


/**
 * Custom Hook folder
 */
require get_template_directory() . '/inc/hook/hook-template.php';


function app_disable_emoji_dequeue_script() {
    wp_dequeue_script( 'emoji' );
}
add_action( 'wp_print_scripts', 'app_disable_emoji_dequeue_script', 100 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

// Header Links entfernen
add_action('init', 'app_clean_header');
function app_clean_header() {
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'wp_shortlink_header', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );

    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    wp_deregister_script( 'comment-reply' );
    wp_deregister_script('wp-embed');
}


/**
 * add url for PME pages
 */
add_action('init', function(){
    add_rewrite_rule(
        "pme/([^/]+)/?",
        'index.php?pagename=pme',
        'top');

    add_rewrite_rule(
        "regie-immobiliere/([^/]+)/?",
        'index.php?pagename=regie-immobiliere',
        'top');

    /*add_rewrite_rule(
        "marche-public/([^/]+)/?",
        'index.php?pagename=marche-public',
        'top');*/
});