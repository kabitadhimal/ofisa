<?php
/**
 * For Twig template functions.
 *
 * @package storefront
 */


function get_first_word__($str)
{

	//print_r($str); die;
	$str1 =  strtok($str[0],  ' ');
	$str1 =  $str1.$str[1];
	
	if(isset($str[2]))
		$str1 =  $str1.$str[2];

	return str_replace('!', '', $str1);
}

function get_first_word($str)
{

	//print_r($str); die;

	$replace = array("!", ".");
	$with   = array("", "");

	$str1 =  strtok($str[0],  ' ');
	$str1 =  $str1.$str[1];
	
	if(isset($str[2]))
		$str1 =  $str1.$str[2];

	return str_replace($replace, $with, $str1);
	//return str_replace('!', '', $str1);
}

function cms_bg_color ($color)
{
    if(is_array($color))
    {
        if(isset($color['DarkGery']))
        	return "secondary";
        elseif(isset($color['Grey']))
        	return "alt";
        elseif(isset($color['Red']))
        	return "primary";
        elseif(isset($color['White']))
        	return "default";
    }
}


function get_ateliers($ateliers)
{
	$ateliers = json_decode($ateliers);

	if(!empty($ateliers))
	{
		echo "<strong>Participants</strong>";
		echo "<div class='clearfix ofisa-row'>";
		foreach ($ateliers as $key => $value) {
			
			echo "<div class='col-sm-4 col-xs-12 cms__oneThird-col ofisa__oneThird-col'>";
			echo "Prénom : ".$value->first_name."<br>";
			echo "Nom : ".$value->last_name."<br>";
			echo "Email : ".$value->email_address;

			if(!empty($value->ateliers))
			{
				foreach($value->ateliers as $atelier_name => $atelier_types)
				{

					//print_r($atelier_types);
					echo "<br><br> ATELIERS : ".str_replace('--', " ", $atelier_name)."<br>";
					if(!empty($atelier_types))
					{
						foreach($atelier_types as $atelier_type)
						{
							echo $atelier_type."<br>";
						}

					}
					
				}
			}

			echo "</div>";

		}
		echo "</div>";
	}

	//echo "<pre>";
	//print_r($ateliers);
}