<?php
/* Template Name: Cms Template */
$fields = get_field_objects();
$team_grid_categories = @$fields['cms_page']['layouts']['14']['sub_fields']['2']['sub_fields']['3']['choices'];
if(!empty($team_grid_categories) )
	$team_grid_categories = $team_grid_categories ;
else
{
	$team_grid_categories =  array(

							    	'ap' => 'Administrations Publiques',
									're' => 'Régie',
									'pme' => 'PME',
									'su' => 'Support',
									'st' => 'Service technique',
									'ad' => 'Administration',
									
								   /* 'mp' => 'Marché public',
								    'ri' => 'Régie',
								    'pme' => 'PME',
								    'st' => 'Service technique',
								    'ad' => 'administration'*/
								);
}

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['team_grid_categories'] = $team_grid_categories;

Timber::render( array( 'templates/cms.twig' ), $context );