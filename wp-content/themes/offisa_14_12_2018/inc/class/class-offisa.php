<?php
/**
 * Storefront Class
 *
 * @author   WooThemes
 * @since    2.0.0
 * @package  storefront
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'Offisa' ) ) :
	/**
	 * The main Storefront class
	 */
	class Offisa {
		private static $structured_data;
		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'after_setup_theme',     array( $this, 'setup' ) );
			add_action( 'widgets_init',          array( $this, 'widgets_init' ) );
			add_action( 'wp_enqueue_scripts',    array( $this, 'scripts' ),       10 );

			add_filter( 'body_class',            array( $this, 'body_classes' ) );
			add_action( 'wp_footer',             array( $this, 'get_structured_data' ) );

			$this->add_required_files();
			
			add_action( 'init', array( $this, 'register_post_type' ) );

		}
		
		
		public function register_post_type(){

			/**
			 * Register a book post type.
			 *
			 * @link http://codex.wordpress.org/Function_Reference/register_post_type
			 */

				$labels = array(
						'name'               => _x( 'Invitations', 'post type general name', 'offisa-theme' ),
						'singular_name'      => _x( 'Invitation', 'post type singular name', 'offisa-theme' ),
						'menu_name'          => _x( 'Invitations', 'admin menu', 'offisa-theme' ),
						'name_admin_bar'     => _x( 'Invitation', 'add new on admin bar', 'offisa-theme' ),
						'add_new'            => _x( 'Add New', 'book', 'offisa-theme' ),
						'add_new_item'       => __( 'Add New Invitation', 'offisa-theme' ),
						'new_item'           => __( 'New Invitation', 'offisa-theme' ),
						'edit_item'          => __( 'Edit Invitation', 'offisa-theme' ),
						'view_item'          => __( 'View Invitation', 'offisa-theme' ),
						'all_items'          => __( 'All Invitations', 'offisa-theme' ),
						'search_items'       => __( 'Search Invitations', 'offisa-theme' ),
						'parent_item_colon'  => __( 'Parent Invitations:', 'offisa-theme' ),
						'not_found'          => __( 'No books found.', 'offisa-theme' ),
						'not_found_in_trash' => __( 'No books found in Trash.', 'offisa-theme' )
					);

				$args = array(
					'labels'             => $labels,
			        'description'        => __( 'Description.', 'offisa-theme' ),
					'public'             => true,
					'publicly_queryable' => true,
					'show_ui'            => true,
					'show_in_menu'       => true,
					'query_var'          => true,
					'rewrite'            => array( 'slug' => 'invitation' ),
					'capability_type'    => 'post',
					'has_archive'        => true,
					'hierarchical'       => false,
					'menu_position'      => null,
					'supports'           => array( 'title', 'editor', 'archives' ),
					
				);

				register_post_type( 'invitation', $args );


				// Add new taxonomy, NOT hierarchical (like tags)
				$labels = array(
					'name'                       => _x( 'Event Type', 'taxonomy general name', 'textdomain' ),
					'singular_name'              => _x( 'Event Type', 'taxonomy singular name', 'textdomain' ),
					'search_items'               => __( 'Search Event Type', 'textdomain' ),
					'popular_items'              => __( 'Popular Event Type', 'textdomain' ),
					'all_items'                  => __( 'All Event Type', 'textdomain' ),
					'parent_item'                => null,
					'parent_item_colon'          => null,
					'edit_item'                  => __( 'Edit Event Type', 'textdomain' ),
					'update_item'                => __( 'Update Event Type', 'textdomain' ),
					'add_new_item'               => __( 'Add New Event Type', 'textdomain' ),
					'new_item_name'              => __( 'New Event Type Name', 'textdomain' ),
					'separate_items_with_commas' => __( 'Separate Event Type with commas', 'textdomain' ),
					'add_or_remove_items'        => __( 'Add or remove Event Type', 'textdomain' ),
					'choose_from_most_used'      => __( 'Choose from the most used Event Type', 'textdomain' ),
					'not_found'                  => __( 'No Event Type found.', 'textdomain' ),
					'menu_name'                  => __( 'Event Type', 'textdomain' ),
				);

				$args = array(
								'hierarchical'          => true,
								'labels'                => $labels,
								'show_ui'               => true,
								'show_admin_column'     => true,
								'update_count_callback' => '_update_post_term_count',
								'query_var'             => true,
								'rewrite'               => array( 'slug' => 'event_type' ),
							);

				register_taxonomy( 'event_type', 'invitation', $args );


		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		public function setup() {
			/*
			 * Make theme available for translation.
			 * Translations can be filed in the /languages/ directory.
			 * If you're building a theme based on components, use a find and replace
			 * to change 'offisa' to the name of your theme in all the template files.
			 */
			load_theme_textdomain( 'offisa', get_template_directory() . '/languages' );

			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );

			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );

			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );

			add_image_size( 'offisa-featured-image', 640, 9999 );

			// This theme uses wp_nav_menu() in one location.
			/*register_nav_menus( array(
				'menu-1' => esc_html__( 'Top', 'offisa' ),
				) );*/

			register_nav_menus( array(
				'top-left-menu' => esc_html__( 'Top Left Menu', 'offisa' ),
				) );
			register_nav_menus( array(
				'top-right-menu' => esc_html__( 'Top Right Menu', 'offisa' ),
				) );

			/**
			 * Add support for core custom logo.
			 */
			add_theme_support( 'custom-logo', array(
				'height'      => 200,
				'width'       => 200,
				'flex-width'  => true,
				'flex-height' => true,
			) );

			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

			// Set up the WordPress core custom background feature.
			add_theme_support( 'custom-background', apply_filters( 'offisa_custom_background_args', array(
				'default-color' => 'ffffff',
				'default-image' => '',
			) ) );

			if( function_exists('acf_add_options_page') ) {
	
				acf_add_options_page();
				
			}
		}
		/**
		 * Register widget area.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
		 */
		public function widgets_init() {
			register_sidebar( array(
				'name'          => esc_html__( 'Sidebar', 'offisa' ),
				'id'            => 'sidebar-1',
				'description'   => '',
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			) );
			
		}
		/**
		 * Enqueue scripts and styles.
		 *
		 * @since  1.0.0
		 */
		public function scripts() {

			//wp_enqueue_style( 'offisa-style', get_stylesheet_uri() );
			//header style
			wp_enqueue_style( 'offisa-style', get_template_directory_uri() . '/css/style.css' );


			// header scripts 
			wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-2.2.3.min.js', array(), '20151215', false );
			wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array(), '20151215', false );

			wp_enqueue_script( 'easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array(), '20151215', false );

			wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '20151215', false );

			wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/jquery.waypoints.js', array(), '20151215', false );
			wp_enqueue_script( 'ihead-script', get_template_directory_uri() . '/js/ihead-script.js', array(), '20151215', false );



//footer scripts
			wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20151215', true );
			wp_enqueue_script( 'respond', get_template_directory_uri() . '/js/respond.js', array(), '20151215', true );
			wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/js/smoothscroll.min.js', array(), '20151215', true );
			wp_enqueue_script( 'niceSelect', get_template_directory_uri() . '/js/jquery.niceSelect.js', array(), '20151215', true );
			wp_enqueue_script( 'nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.js', array(), '20151215', true );
			wp_enqueue_script( 'swiper', get_template_directory_uri() . '/js/swiper.min.js', array(), '20151215', true );
			wp_enqueue_script( 'iscript', get_template_directory_uri() . '/js/iscript.js', array(), '20151215', true );


			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}
		}

		/**
		 * Adds custom classes to the array of body classes.
		 *
		 * @param array $classes Classes for the body element.
		 * @return array
		 */
		public function body_classes( $classes ) {
			// Adds a class of group-blog to blogs with more than 1 published author.
			if ( is_front_page() ) {
				$classes[] = 'ofisa-homepage-template';
			}
			else if( is_page_template( 'sd-templates/template-cms.php' ))
			{
				$classes[] = 'ofisa-innerpage-template ofisa__cms-template'; 
			}
			
			return $classes;
		}

		/**
		 * Check if the passed $json variable is an array and store it into the property...
		 */
		public static function set_structured_data( $json ) {
			if ( ! is_array( $json ) ) {
				return;
			}
			self::$structured_data[] = $json;
		}
		/**
		 * If self::$structured_data is set, wrap and echo it...
		 * Hooked into the `wp_footer` action.
		 */
		public function get_structured_data() {
			if ( ! self::$structured_data ) {
				return;
			}
			$structured_data['@context'] = 'http://schema.org/';
			if ( count( self::$structured_data ) > 1 ) {
				$structured_data['@graph'] = self::$structured_data;
			} else {
				$structured_data = $structured_data + self::$structured_data[0];
			}
			$structured_data = $this->sanitize_structured_data( $structured_data );
			echo '<script type="application/ld+json">' . wp_json_encode( $structured_data ) . '</script>';
		}
		/**
		 * Sanitize structured data.
		 *
		 * @param  array $data
		 * @return array
		 */
		public function sanitize_structured_data( $data ) {
			$sanitized = array();
			foreach ( $data as $key => $value ) {
				if ( is_array( $value ) ) {
					$sanitized_value = $this->sanitize_structured_data( $value );
				} else {
					$sanitized_value = sanitize_text_field( $value );
				}
				$sanitized[ sanitize_text_field( $key ) ] = $sanitized_value;
			}
			return $sanitized;
		}

		function add_required_files() {
			/**
			 * Custom template tags for this theme.
			 */
			require get_template_directory() . '/inc/template-tags.php';

			/**
			 * Custom functions that act independently of the theme templates.
			 */
			require get_template_directory() . '/inc/extras.php';

			/**
			 * Customizer additions.
			 */
			require get_template_directory() . '/inc/customizer.php';

			/**
			 * Load Jetpack compatibility file.
			 */
			require get_template_directory() . '/inc/jetpack.php';

		}
	}
endif;
return new Offisa();