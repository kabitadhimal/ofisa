<?php
 
 function button_shortcode( $atts,$content) {

        $a = shortcode_atts( array(   'link' => "" ,"target" => "" ), $atts );

        $link = $a['link'];
        $target = $a['target'];

        $html = '<a class="btn btn-default btn-red" href="'.$link.'" target="'.$target.'">'.$content.'</a>';
        //echo $html; die('die');
        return $html;
    }

    add_shortcode( 'custom_button', 'button_shortcode' );