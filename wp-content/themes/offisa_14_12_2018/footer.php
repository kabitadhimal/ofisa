<!-- Ofisa footer -->

<footer id="ofisa-colphon" class="ofisa-footer ofisa-main-footer" role="contentinfo">
	<div class="ofisa-footer-row">
		<div class="ofisa-container ofisa__sub-container">

			<!-- Ofisa footer col wrapper row -->
			<div class="clearfix ofisa-row">

				<div class="col-sm-4 col-xs-12 ofisa__footer-col">
					<div class="ofisa-col-content">

						<!-- Ofisa footer header title -->
						<div class="footer__col-header">
							<figure class="ofisa__footer-logo">
								<a href=""><img src="<?php bloginfo('template_url'); ?>/images/footer-logo.png" alt="" /></a>
							</figure>
						</div><!-- /.#Ofisa footer header title -->

						<p>Small text paragraph lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						<span class="mail-info-text">
							<a href="mailto:info@o-i.ch">info@o-i.ch</a>
						</span>

					</div>
				</div>

				<div class="col-sm-3 col-xs-12 ofisa__footer-col">
					<div class="ofisa-col-content">

						<!-- Ofisa footer header title -->
						<div class="footer__col-header">
							<h5>H5 Title Lorem</h5>
						</div><!-- /.#Ofisa footer header title -->

						<p>Small text paragraph</p>
						<p>Small text paragraph</p>
						<p>Small text paragraph</p>
						<span class="mail-info-text">
							<a href="mailto:info@o-i.ch">info@o-i.ch</a>
						</span>

					</div>
				</div>

				<div class="col-sm-5 col-xs-12 ofisa__footer-col">
					<div class="ofisa-col-content">

						<!-- Ofisa footer header title -->
						<div class="footer__col-header">
							<h5>H5 Title Lorem</h5>
						</div><!-- /.#Ofisa footer header title -->

						<ul class="footer__logo-group">
							<li class="footer__logo-item"><a href=""><img src="<?php bloginfo('template_url'); ?>/images/logo-nest.png" alt="" /></a></li>
							<li class="footer__logo-item"><a href=""><img src="<?php bloginfo('template_url'); ?>/images/logo-abacus.png" alt="" /></a></li>
							<li class="footer__logo-item"><a href=""><img src="<?php bloginfo('template_url'); ?>/images/logo-diamond.png" alt="" /></a></li>
						</ul>

					</div>
				</div>

			</div><!-- /.#Ofisa footer col wrapper row -->

		</div>
	</div>
</footer>

<!-- /.#Ofisa footer -->

</div><!-- /.#Ofisa inner wrapper -->
</div><!-- /.#Ofisa outer wrapper -->

<!-- Ofisa back to top -->

<div class="ofisa__backTop-wrapper">
	<a href="" id="ofisa__backTop" class="ofisa__backTop"><i class="top__arrow"></i></a>
</div><!-- /.#Ofisa back to top -->

<!-- Importing script files -->

<!-- footer scripts -->

<!-- /. #Importing script files -->
<?php wp_footer(); ?>
</body>
</html>