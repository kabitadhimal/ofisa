<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Offisa
 */

$context = Timber::get_context();
$post = new TimberPost();

Timber::render( array( 'templates/404.twig' ), $context );