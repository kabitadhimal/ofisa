<?php
/* Template Name: Cms Template */
$fields = get_field_objects();
$team_grid_categories = @$fields['cms_page']['layouts']['14']['sub_fields']['2']['sub_fields']['3']['choices'];
if(!empty($team_grid_categories) )
	$team_grid_categories = $team_grid_categories ;
else
{
	$team_grid_categories =  array(

							    	'ap' => 'Administrations Publiques',
									're' => 'Régie',
									'pme' => 'PME',
									'su' => 'Support',
									'st' => 'Service technique',
									'ad' => 'Administration',
									
								   /* 'mp' => 'Marché public',
								    'ri' => 'Régie',
								    'pme' => 'PME',
								    'st' => 'Service technique',
								    'ad' => 'administration'*/
								);
}

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['team_grid_categories'] = $team_grid_categories;

function strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }


function selfURL() {
    if (!isset($_SERVER['REQUEST_URI'])) {
        $serverrequri = $_SERVER['PHP_SELF'];
    } else {
        $serverrequri = $_SERVER['REQUEST_URI'];
    }
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
    //$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    $port="";
    return $protocol."://".$_SERVER['HTTP_HOST'].$port.$serverrequri;
}


$pageUrl = selfURL();
$childUrl = urldecode(str_replace(get_the_permalink(), "", $pageUrl));
$childPages = explode("/",$childUrl);

$context['pageName'] = ($childPages[0]) ? $childPages[0] : '';
$context['childPageName'] = ($childPages[1]) ? $childPages[1] : '';



Timber::render( array( 'templates/cms.twig' ), $context );