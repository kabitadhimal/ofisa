<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['top_left_menu'] =  array(
    'theme_location' => 'top-left-menu',
    //'menu_class' => 'menu__styled-list-group' ,
    'items_wrap'     => '<ul class="menu__styled-list-group" role="menu">%3$s</ul>'
);
$context['top_right_menu'] =  array(
    'theme_location' => 'top-right-menu',
    'items_wrap'     => '<ul class="menu__simple-list-group" role="menu">%3$s</ul>'
);
$blogs = Timber::get_posts();
$context['post'] = $post;
$context['blogs'] = $blogs;
$context['categories'] = get_categories();
$context['posts_query'] = new Timber\PostQuery();
$context['latest_title'] = __('The latest news', 'offisa');
$context['ajax_url'] = admin_url( 'admin-ajax.php' );
$context['loading'] = get_template_directory_uri() . '/images/loading.gif';

Timber::render( array( 'views/templates/news.twig' ), $context );
