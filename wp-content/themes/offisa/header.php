<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="msvalidate.01" content="CB8EAEA2ECCE23041DFA315FC6CEFBCC" />
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
          <!-- #Bootstrap -->
          <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png">
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
          <?php wp_head(); ?>
	
      </head>
      <body class="ofisa-homepage-template" <?php body_class(); ?>>
      	<div id="ofisa-wrapper" class="ofisa-wrapper" role="main">
      		<div id="ofisa-inner-wrapper" class="ofisa-inner-wrapper">

      			<header id="masthead" class="ofisa-masthead ofisa__mainHeader" role="banner">
      				<div class="ofisa-header-row">
      					<div class="ofisa-container ofisa__main-container">
      						<div class="row clearfix ofisa__row-fixer">
      							<div class="col-sm-2 col-xs-12 ofisa__valign-middle ofisa__logo-col">
      								<figure class="ofisa__logo">
      									<a href="">
      									<img src="<?php bloginfo('template_url'); ?>/images/logo.svg" alt="" class="ofisa__valign-top" />
      									</a>
      								</figure>
      							</div>

      							<!-- Ofisa main navigation col -->
      							<div class="col-sm-10 col-xs-12 ofisa__valign-middle ofisa__navigation-col">
      								<!-- Ofisa and Supports Brand and toggle get grouped for better mobile display -->
      								<div class="navbar-header ofisa__navbar-header">
      									<a href="" class="navbar-toggle closer" data-target="ofisa__mobile-menu">
      										<span class="icon-bar"></span>
      										<span class="icon-bar"></span>
      										<span class="icon-bar"></span>
      									</a>
      								</div><!-- /.#Ofisa and Supports Brand and toggle get grouped for better mobile display -->

      								<!-- Ofisa mobile nav -->
      								<nav class="ofisa__mobile-menu ofisa__mobilenav-wrap" role="navigation">
      									<!-- Ofisa header navigation col row wrapper -->
      									<div class="row clearfix ofisa__row-fixer">
      										<!-- Ofisa header navigation left list group col -->
      										<div class="col-sm-5 col-xs-12 ofisa__valign-middle ofisa__list-group-col ofisa__left-list-group">
      											<ul class="menu__styled-list-group" role="menu">
      												<li class="active"><a href="">Marché Public</a></li>
      												<li><a href="">Régie immobilière</a></li>
      												<li><a href="">PME</a></li>
      											</ul>
      										</div><!-- /.#Ofisa header navigation left list group col -->
      										<!-- Ofisa header navigation right list group col -->
      										<div class="col-sm-7 col-xs-12 ofisa__valign-middle ofisa__list-group-col ofisa__right-list-group">
      											<ul class="menu__simple-list-group" role="menu">
      												<li class="active"><a href="">Service technique</a></li>
      												<li><a href="">A Propos</a></li>
      												<li><a href="">Contact</a></li>
      											</ul>
      										</div><!-- /.#Ofisa header navigation rightt list group col -->
      									</div><!-- /#Ofisa header navigation col row wrapper -->
      								</nav><!-- /.#Ofisa mobile nav -->

      								<!-- Header floating social icons -->
      								<div class="ofisa__header-social-row">
      									<ul id="ofisa__social-group" class="ofisa__social-group">
      										<li class="ofisa__twiter-item"><a href=""><i class="icon icon-twitter"></i></a></li>
      										<li class="ofisa__linkedin-item"><a href=""><i class="icon icon-linkedin"></i></a></li>
      									</ul>
      								</div><!-- /.#Header floating social icons -->
      							</div>
      							<!-- /.#Ofisa navigation logo col -->
      						</div><!-- /.#Ofisa header col wrapper row -->
      					</div>
      				</div>
                </header><!-- /.#Ofisa header -->