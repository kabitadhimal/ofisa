<?php
/* Template Name: Home Template */ 
$context = Timber::get_context();
$post = new TimberPost();
$context['top_left_menu'] =  array(
                                'theme_location' => 'top-left-menu',
                                //'menu_class' => 'menu__styled-list-group' ,
                                'items_wrap'     => '<ul class="menu__styled-list-group" role="menu">%3$s</ul>'
                            );
$context['top_right_menu'] =  array(
                                'theme_location' => 'top-right-menu',
                                'items_wrap'     => '<ul class="menu__simple-list-group" role="menu">%3$s</ul>'
                            );
$context['post'] = $post;
Timber::render( array( 'templates/home.twig' ), $context );
