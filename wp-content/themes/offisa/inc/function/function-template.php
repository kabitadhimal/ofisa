<?php
/**
 * For Twig template functions.
 *
 * @package storefront
 */


function get_first_word__($str)
{

	//print_r($str); die;
	$str1 =  strtok($str[0],  ' ');
	$str1 =  $str1.$str[1];
	
	if(isset($str[2]))
		$str1 =  $str1.$str[2];

	return str_replace('!', '', $str1);
}

function get_first_word($str)
{

	//print_r($str); die;

	$replace = array("!", ".");
	$with   = array("", "");

	$str1 =  strtok($str[0],  ' ');
	$str1 =  $str1.$str[1];
	
	if(isset($str[2]))
		$str1 =  $str1.$str[2];

	return str_replace($replace, $with, $str1);
	//return str_replace('!', '', $str1);
}

function cms_bg_color ($color)
{
    if(is_array($color))
    {
        if(isset($color['DarkGery']))
        	return "secondary";
        elseif(isset($color['Grey']))
        	return "alt";
        elseif(isset($color['Red']))
        	return "primary";
        elseif(isset($color['White']))
        	return "default";
    }
}


function get_ateliers($ateliers)
{
	$ateliers = json_decode($ateliers);

	if(!empty($ateliers))
	{
		echo "<strong>Participants</strong>";
		echo "<div class='clearfix ofisa-row'>";
		foreach ($ateliers as $key => $value) {
			
			echo "<div class='col-sm-4 col-xs-12 cms__oneThird-col ofisa__oneThird-col'>";
			echo "Prénom : ".$value->first_name."<br>";
			echo "Nom : ".$value->last_name."<br>";
			echo "Email : ".$value->email_address;

			if(!empty($value->ateliers))
			{
				foreach($value->ateliers as $atelier_name => $atelier_types)
				{

					//print_r($atelier_types);
					echo "<br><br> ATELIERS : ".str_replace('--', " ", $atelier_name)."<br>";
					if(!empty($atelier_types))
					{
						foreach($atelier_types as $atelier_type)
						{
							echo $atelier_type."<br>";
						}

					}
					
				}
			}

			echo "</div>";

		}
		echo "</div>";
	}

	//echo "<pre>";
	//print_r($ateliers);
}

if(!function_exists('page_arrange_parentChild')){
    function page_arrange_parentChild($parent_id = 0)
    {
        $args = array(
            'sort_order' => 'asc',
            'sort_column' => 'menu_order',
            'post_type' => 'page',
            'post_status' => 'publish',
            'child_of' => $parent_id
        );
        $data = get_pages( $args );
        $parent_data = array();
        foreach($data as $page){
            if( $parent_id == $page->post_parent ){
                $parent_data[$page->ID]['main_title'] = $page->post_title;
                $parent_data[$page->ID]['main_content'] = $page->post_content;
                $parent_data[$page->ID]['main_permalink'] = get_permalink($page->ID);
                $parent_data[$page->ID]['main_data'] = $page;
            }
        }
        foreach($parent_data as $key=>$value){
            $args = array(
                'sort_order' => 'asc',
                'sort_column' => 'menu_order',
                'post_type' => 'page',
                'post_status' => 'publish',
                'child_of' => $key
            );
            $page_data = get_pages( $args );
            foreach($page_data as $child_page){
                $parent_data[$key]['sub_content'][$child_page->ID]['sub_title'] = $child_page->post_title;
                $parent_data[$key]['sub_content'][$child_page->ID]['sub_content'] = $child_page->post_content;
                $parent_data[$key]['sub_content'][$child_page->ID]['sub_permalink'] =  get_permalink($child_page->ID);
                $parent_data[$key]['sub_content'][$child_page->ID]['sub_data'] = $child_page;
            }
        }
        return $parent_data;
    }
}

if( !function_exists( 'first_child_permalink' ) ){
    function first_child_permalink( $data = array() ){
        if( !empty( $data ) ){
            if( key_exists('sub_content', $data) && !empty( $data['sub_content'] ) ){
                foreach( $data['sub_content'] as $item ){
                    echo $item['sub_permalink'];
                    break;
                }
            }else{
                echo $data['main_permalink'];
            }
        }
    }
}

function get_image_url($attachment_id)
{
    return wp_get_attachment_url( $attachment_id );
}

function get_all_category_as_string($post_id){
    $category_detail = get_the_category($post_id);
    if($category_detail){
        $category_detail = implode(' ', array_map(function($a){return $a->slug;},$category_detail));
    }
    return $category_detail;
}
function post_sigle_page_template_redirect()
{
    if( is_singular('post') )
    {
        wp_redirect( get_permalink(get_option( 'page_for_posts' )), 301 );
        die;
    }
}
add_action( 'template_redirect', 'post_sigle_page_template_redirect' );