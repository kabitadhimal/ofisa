<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}


Timber::$dirname = array('templates', 'views');

class OffisaSite extends TimberSite {

	function __construct() {
		/*add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );*/
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		add_filter( 'timber_context', 'mytheme_timber_context'  );

 		$context['options'] = get_fields('option');
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;

		$context['top_left_menu'] =  array(
                                'theme_location' => 'top-left-menu',
                                //'menu_class' => 'menu__styled-list-group' ,
                                'items_wrap'     => '<ul class="menu__styled-list-group" role="menu">%3$s</ul>'
                            );

		$context['top_right_menu'] =  array(
                                'theme_location' => 'top-right-menu',
                                'items_wrap'     => '<ul class="menu__simple-list-group" role="menu">%3$s</ul>'
                            );



		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new OffisaSite();