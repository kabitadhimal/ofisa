<?php


add_action('wp_ajax_invitation_submit', 'invitation_submit');
add_action('wp_ajax_nopriv_invitation_submit', 'invitation_submit');

function invitation_submit()
{

    $error = [];

    $datas = $_POST;
    parse_str($datas['datas'], $submited_datas);


    $title_of_checkbox = $submited_datas['title_of_checkbox'];

    $type = $submited_datas['type'];

    $enterprise = $submited_datas['enterprise'];
    $address = $submited_datas['address'];
    $location = $submited_datas['location'];
    $telephone = $submited_datas['telephone'];

    $first_names = $submited_datas['first_name'];
    $last_names = $submited_datas['last_name'];
    $email_address = $submited_datas['email_address'];
    $ateliers = $submited_datas['ateliers'];


    if (empty($enterprise))
        $error['enterprise'] = 'Le champ Enterprise est requis.';

    if (empty($address))
        $error['address'] = 'Le champ Entreprise Adresse est requis.';

    if (empty($location))
        $error['location'] = 'Le champ Localité est requis.';

    if (empty($telephone))
        $error['telephone'] = 'Le champ Téléphone est requis.';

    /*	if(empty($first_names))
            $error['first_names'] = 'First Name est requis.';

        if(empty($last_names))
            $error['last_names'] = 'Last Name est requis.';

        if(empty($email_address))
            $error['email_address'] = 'Email Address est requis.';*/


    $ateliers_new = [];
    if (!empty($ateliers)) {
        $count = 1;
        foreach ($ateliers as $k1 => $atelier_arr) {
            foreach ($atelier_arr as $atelier) {
                $val = explode('-', $atelier);

                if (isset($val[2])) {
                    $key = $val[2] - 1;
                    $ateliers_new[$k1][$key][] = $val[0];
                } else {
                    $key = $val[1] - $count;
                    $ateliers_new[$k1][$key][] = $val[0];
                }
            }

            $count++;
        }
    }


    $new_filtered_array = [];

    if (!empty($first_names)
        && ($title_of_checkbox == "Nous acceptons votre invitation avec plaisir.")
    ) {
        foreach ($first_names as $k => $first_name) {


            if (empty($first_names[$k]))
                $error['first_names'] = 'Le champ Prénom est requis.';

            if (empty($last_names[$k]))
                $error['last_names'] = 'Le champ Nom est requis.';

            if (empty($email_address[$k]))
                $error['email_address'] = 'Le champ Email Address est requis.';
            else if (!filter_var($email_address[$k], FILTER_VALIDATE_EMAIL)) {
                $error['email_address'] = "Invalid email format";
            }


            $new_filtered_array[$k] = array('first_name' => $first_names[$k],
                'last_name' => $last_names[$k],
                'email_address' => $email_address[$k],
                //'ateliers' =>
            );


            if (!empty($ateliers_new)) {
                foreach ($ateliers_new as $atelier_name => $atelier_type) {
                    $val = explode('-', $atelier);
                    $new_filtered_array[$k]['ateliers'][$atelier_name] = $atelier_type[$k];
                }
            }
        }


    } else {
        /*echo json_encode(array('status' => 'fail','msg' => "No data to input."));
        die();*/
    }

    if (!empty($error)) {
        echo json_encode(array('status' => 'errors', 'errors' => $error));
        die();
    }


    $my_post = array(
        'post_title' => wp_strip_all_tags($enterprise),
        'post_status' => 'publish',
        'post_type' => 'invitation',
        'post_author' => 1
    );

    // Insert the post into the database
    $post_id = wp_insert_post($my_post);

    wp_set_object_terms($post_id, $type, "event_type");

    add_post_meta($post_id, 'top_check_box', $title_of_checkbox, false);
    add_post_meta($post_id, 'address', $address, false);
    add_post_meta($post_id, 'location', $location, false);
    add_post_meta($post_id, 'enterprise', $enterprise, false);
    add_post_meta($post_id, 'telephone', $telephone, false);
    add_post_meta($post_id, 'ateliers', json_encode($new_filtered_array), false);

    $link = get_the_permalink($post_id);

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    $admin_html = "Une nouvelle entreprise a répondu à l'invitation au club des utilisateurs.<br>
	                Type d'invitation : {EVENT_TYPE}<br>    
					Nom : {ENTERPRISE_NAME}<br>
					Adresse : {ADDRESS}<br>
					Localité : {LOCALITE}<br>
					Téléphone : {TELEPHONE} <br>
					Réponse : {RESPONSE}.<br>";
    //	<a href='".$link."'>Voir en détail la réponse</a></link>";

    $participant_html = "Bonjour {NOM} {PRENOM},<br>
						Merci pour votre inscription aux ateliers suivants :<br><br>
						{ATELIER_INFO}
						<br>
						<br>
						Nous restons à votre disposition pour toute question ou information complémentaire.
						<br><br>
						À bientôt,<br>
						Ofisa Informatique";

    $term = get_term_by('slug', $type, 'event_type');

    $admin_parse_array = array(
        '{ENTERPRISE_NAME}' => $enterprise,
        '{ADDRESS}' => $address,
        '{LOCALITE}' => $location,
        '{TELEPHONE}' => $telephone,
        '{RESPONSE}' => $title_of_checkbox,
        '{EVENT_TYPE}' => $term->name,
    );
    //	var_dump($new_filtered_array);
    $AteliersAdminHtml = "";
    if (!empty($new_filtered_array)) {
        $AteliersAdminHtml .= "<strong>Participants</strong>";
        $AteliersAdminHtml .= "<div class='clearfix ofisa-row'>";
        foreach ($new_filtered_array as $key => $value) {
            $AteliersAdminHtml .= "<div class='col-sm-4 col-xs-12 cms__oneThird-col ofisa__oneThird-col'>";
            if (!empty($value['first_name'])):
                $AteliersAdminHtml .= "Prénom : " . $value['first_name'] . "<br>";
            endif;

            if (!empty($value['first_name'])):
                $AteliersAdminHtml .= "Nom : " . $value['last_name'] . "<br>";
            endif;

            if (!empty($value['first_name'])):
                $AteliersAdminHtml .= "Email : " . $value['email_address'] . "<br/>";
            endif;
            $ateliersList = $value['ateliers'];

            if (!empty($ateliersList)) {
                foreach ($ateliersList as $atelier_types) {
                    if (!empty($atelier_types)) {
                        $AteliersAdminHtml .= '<strong>ATELIERS : ' . "</strong><br/>";
                        foreach ($atelier_types as $atelier_type) {
                            $AteliersAdminHtml .= $atelier_type . "<br>";
                        }
                    }
                }
            }
            $AteliersAdminHtml .= "</div>";

        }
        $AteliersAdminHtml .= "</div>";
    }

    $admin_email_content = strtr($admin_html, $admin_parse_array);
    $admin_email_content .= $AteliersAdminHtml;

    $to = get_option('admin_email');
    //$to = 'developersubodh10@gmail.com';
    //echo $to;
    $subject = 'Offisa Invitation';
    $adminEmail = get_field("admin_email", "option");
    if (!empty($adminEmail)) {
        $adminEmailID = $adminEmail;

    } else {
        $adminEmailID = $to;
    }
    mail($adminEmailID, $subject, $admin_email_content, $headers);


    $atlers = get_ateliers($new_filtered_array);


    if (!empty($new_filtered_array)) {


        foreach ($new_filtered_array as $key => $value) {

            $atler_html = "";
            if (!empty($value['ateliers'])) {
                foreach ($value['ateliers'] as $atelier_name => $atelier_types) {
                    $atler_html .= str_replace('--', " ", $atelier_name) . "  : ";
                    if (!empty($atelier_types)) {
                        $atler_type_html = implode(",", $atelier_types);
                        $atler_html .= $atler_type_html . "<br>";
                    }
                }
            }

            //var_dump($atler_html);

            $participant_parse_array = array(
                '{NOM}' => $value['first_name'],
                '{PRENOM}' => $value['last_name'],
                '{ATELIER_INFO}' => $atler_html
            );

            $participant_email_content = strtr($participant_html, $participant_parse_array);


            $senderEmail = get_field("sender_email", "option");

            if (!empty($senderEmail)) {
                $sendEmailTo = $senderEmail;
            } else {
                $sendEmailTo = $value['email_address'];
            }

            $subject = 'Ofisa Invitation';

            mail($sendEmailTo, $subject, $participant_email_content, $headers);

        }
    }

    echo json_encode(array('status' => 'success', 'msg' => "Merci pour votre inscription. Un e-mail de confirmation vous sera envoyé dans les prochains jours."));
    die();


}


function build_email_template($parse_data = '', $name = '')
{
    //$template = file_get_contents_curl(TEMP_DIR_URI . '/partial/email-templates/' . $lang . '/' . $type . '.html');
    $template = file_get_contents_curl(site_url() . '/email-template/' . $name . '.html');
    // echo site_url() .'/'. $type . '.html';die('testing');

    // echo TEMP_DIR_URI . '/partial/email-templates/' . $lang . '/' . $type . '.html';die();
    $email_content = strtr($template, $parse_data);
    return $email_content;
}


function file_get_contents_curl($url)
{
    $ch = curl_init();

//    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

    $data = curl_exec($ch);
    return $data;


}

add_action('wp_ajax_filter_category_data', 'filter_category_data');
add_action('wp_ajax_nopriv_filter_category_data', 'filter_category_data');

function filter_category_data()
{
    $paged = $_POST['paged']?$_POST['paged']:1;
    $category_slug = $_POST['category_slug'];
    $posts_per_page = get_option('posts_per_page');
    $args = array('posts_per_page' => $posts_per_page, 'category_name' => $category_slug, 'paged' => $paged);
    if($category_slug == 'all'){
        $args = array('posts_per_page' => $posts_per_page,'paged' => $paged,);
    }
    /*$posts = get_posts($args);
    foreach ($posts as $post):
        setup_postdata($post);
    endforeach;*/
    if (!isset($paged) || !$paged){
        $paged = 1;
    }
    $context = Timber::context();
    $context['posts'] = new Timber\PostQuery($args);
    $posts = Timber::get_posts($args);
    $context['blogs'] = $posts;
    $context['loading'] = get_template_directory_uri() . '/images/loading.gif';
    Timber::render( array( 'views/templates/ajax-news.twig' ), $context );
    exit;
}