<?php
function redirect_not_found_page_to_homepage(){
    if( is_404() ){
        $current_url = urldecode ("//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        if (strpos($current_url, '/net/Info.asp?NoOFS=8105&NumStr=home') !== false) {
            $url = home_url();
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: $url");
            exit();
        }
    }
}
add_action( 'template_redirect', 'redirect_not_found_page_to_homepage' );

define('TEMPATH', get_bloginfo('template_directory'));
define('IMAGE', TEMPATH . "/images");
define('CSS', TEMPATH . "/css");
define('SCRIPT', TEMPATH . "/js");
require get_template_directory() . '/inc/init.php';

/*
add_action('init', function() {
    $url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');
    //ie();
    if ( strpos($url_path, 'pme') !== false) {
        global $post;
        $post = get_post(706);
        $load = locate_template('sd-templates/template-cms.php', true);
        if ($load) {
            exit(); // just exit if template was found and loaded
        }
   }
});
*/

/**
 * Google recaptcha
 * remove recaptcha from unnecessary page
 */
remove_action( 'wp_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts' );
if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
    add_action( 'wp_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts', 10, 0 );
}
function oiw_load_recaptcha_badge_page(){
    if ( !is_page_template('sd-templates/template-pme.php') && !is_page( array( 'contact-page','pme', 'support' ) ) ) {
        wp_dequeue_script('google-recaptcha');
    }
}
add_action( 'wp_enqueue_scripts', 'oiw_load_recaptcha_badge_page' );

function hook_css() {
    ?>
    <style>
        .grecaptcha-badge{
            display: none;
        }
    </style>
    <?php
}
add_action('wp_head', 'hook_css');